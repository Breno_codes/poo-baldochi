## Leia valores numéricos e os coloque numa lista.
## A leitura termina quando o valor 0 for digitado. Em seguida,
## calcule a média dos valores digitados e informe o
## usuário.

listaNum = []
while True:
    num = int(input('Digite um valor numérico: '))
    if num == 0:
        break
    listaNum.append(num)
soma = 0
for nro in listaNum:
    print(nro)
    soma += nro
media = soma / len(listaNum)
print('A média dos numéros digitados é' + str(media))

