## Leia uma string e verifique se a mesma é um
## palíndromo.

name = input('Entre com a string: ')
palin = True
for i in range(0, int(len(name)) / 2):
    if (name[i] != name[len(name) - i -1]):
        palin = False
if palin:
    print(name + ' é palíndromo :D')
else:
    print(name + ' não é palíndromo D:')
